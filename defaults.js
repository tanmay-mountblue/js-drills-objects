module.exports.defaults = (obj,obj2) => {
    if (typeof obj !== "object" || obj === null || typeof obj2 !== "object" || obj2 === null) {
      return "Invalid Input!!!";
    }
    for (let val in obj) {
        if(obj[val] === undefined){
            if(obj2[val]){
                obj[val]=obj2[val];
            }
        }
    }
    return obj;
  };
  