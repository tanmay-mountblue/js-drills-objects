module.exports.pairs = (obj) => {
  if (typeof obj !== "object" || obj === null) {
    return "Invalid Input!!!";
  }
  let arr = [];
  for (let val in obj) {
    arr.push([val, obj[val]]);
  }
  return arr;
};
