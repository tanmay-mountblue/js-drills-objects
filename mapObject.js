module.exports.mapObject = (obj, cb) => {
  if (typeof obj !== "object" || obj === null) {
    return "Invalid Input!!!";
  }
  let arr = [];
  for (let val in obj) {
    let temp = obj[val];
    if (typeof obj[val] === "number") 
      obj[val] = cb(obj[val]);
  }
  return obj;
};
