module.exports.values = (obj) => {
  if (typeof obj !== "object" || obj === null) {
    return "Invalid Input!!!";
  }
  let arr = [];
  for (let val in obj) {
    arr.push(obj[val]);
  }
  return arr;
};
